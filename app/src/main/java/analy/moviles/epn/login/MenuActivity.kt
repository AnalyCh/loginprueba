package analy.moviles.epn.login

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth

class MenuActivity : AppCompatActivity() {

    lateinit var sign_out_button: Button


    companion object {
        fun getLaunchIntent(from: Context) = Intent(from, MenuActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        sign_out_button = findViewById(R.id.btn_logout)
        setupUI()


    }

    private fun setupUI() {
        sign_out_button.setOnClickListener {
            signOut()
        }
    }

    private fun signOut() {
        startActivity(LogInActivity.getLaunchIntent(this))
        FirebaseAuth.getInstance().signOut()
    }
}
